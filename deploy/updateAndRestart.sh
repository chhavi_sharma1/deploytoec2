#!/bin/bash


set -e


rm -rf /home/ubuntu/deploytoec2/

# clone the repo again
git clone https://gitlab.com/chhavi_sharma1/deploytoec2.git

sudo apt-get install nginx -y
sudo service nginx start 

sudo cp /home/ubuntu/deploytoec2/index.html /var/www/html/

sudo service nginx reload
